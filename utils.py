import numpy as np
from tensorflow.keras import backend as K
from skimage.io import imread, imshow

import matplotlib.pyplot as plt
import slidingwindow as sw

def rounddown(x):
    return int(math.floor(x / IMG_WIDTH)) * IMG_WIDTH

def mask_to_image(mask):
    return (np.dstack([mask, mask, mask]) *255.999).astype(np.uint8)

def merge_masks(mask_paths, height, width):
    mask = np.zeros((height, width, 1), dtype=np.bool)
    for mask_file in mask_paths:
        mask_ = imread(mask_file)
        mask_ = np.expand_dims(mask_, axis=-1)
        mask = np.maximum(mask, mask_)
    return mask

def dice_coef(y_true, y_pred):
    smooth = 1.0
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)

def weighted_categorical_crossentropy(weights):
    # weights = [0.9,0.05,0.04,0.01]
    def wcce(y_true, y_pred):
        Kweights = K.constant(weights)
        if not K.is_tensor(y_pred): y_pred = K.constant(y_pred)
        y_true = K.cast(y_true, y_pred.dtype)
        return K.categorical_crossentropy(y_true, y_pred) * K.sum(y_true * Kweights, axis=-1)
    return wcce

def bce_dice_loss(y_true, y_pred):
    return 0.5 * binary_crossentropy(y_true, y_pred) - dice_coef(y_true, y_pred)

def iou_coef(y_true, y_pred, smooth=1):
    intersection = K.sum(K.abs(y_true * y_pred), axis=[1,2,3])
    union = K.sum(y_true,[1,2,3])+K.sum(y_pred,[1,2,3])-intersection
    iou = K.mean((intersection + smooth) / (union + smooth), axis=0)
    return iou

def jacard_coef(y_true, y_pred):
    y_true_f = K.flatten(y_true)
    y_pred_f = K.flatten(y_pred)
    intersection = K.sum(y_true_f * y_pred_f)
    return (intersection + 1.0) / (K.sum(y_true_f) + K.sum(y_pred_f) - intersection + 1.0)


def jacard_coef_loss(y_true, y_pred):
    return -jacard_coef(y_true, y_pred)


def dice_coef_loss(y_true, y_pred):
    return -dice_coef(y_true, y_pred)

def iou(y_true, y_pred, smooth=1):
    intersection = np.sum(np.abs(y_true * y_pred))
    union = np.sum(y_true) + np.sum(y_pred) - intersection
    iou = np.mean(intersection / union)
    return iou


class Tiler(object):
    
    def __init__(self, img):
        self.img = img.copy()
        #self.mask = mask
        self.orig_shape = img.shape
        self.orig_width = img.shape[0]
        self.orig_height = img.shape[1]
        self.tiles = []
        self.windows = []
        self.coords = []
        self.cols = 0
        self.rows = 0
    
    def cut(self, size, overlap=0):
        self.tiles = []
        self.windows = []
        self.coords = []
        self.cols = 0
        self.rows = 0
        
        prev_x = 0
        prev_y = 0
        windows = sw.generate(self.img, sw.DimOrder.HeightWidthChannel, size, overlap)
        for window in windows:
            subset = self.img[ window.indices() ]
            self.tiles.append(subset)
            self.windows.append(window.getRect())
            size_sufix = '_'.join(map(str, window.getRect()))   
            #plt.imsave("temp/"+size_sufix+".png", subset)
            
        self.cols = len(set([a[0] for a in self.windows]))
        self.rows = len(set([a[1] for a in self.windows]))
        
        x = set([a[1] for a in self.windows])
        coord_map_col = {a: i for (i, a) in enumerate(sorted(x))}

        x = set([a[0] for a in self.windows])
        coord_map_row = {a: i for (i, a) in enumerate(sorted(x))}
        for w in self.windows:
            self.coords.append((coord_map_row[w[0]], coord_map_col[w[1]]))
    
    # np.mean( np.array([ old_set, new_set ]), axis=0 )
    def get_merged_image_tiles(self):
        matrix = np.zeros(self.orig_shape)
        for i, tile in enumerate(self.tiles):
            x1 = self.windows[i][0]
            y1 = self.windows[i][1]
            matrix[y1:y1+tile.shape[1], x1:x1+tile.shape[0]] += tile
            
        return matrix
    
    def get_merged_overlapped_tiles(self):
        
        stack_occur_matrix = np.zeros((self.orig_shape[0], self.orig_shape[1], self.tiles.shape[-1]))
        
        matrix = np.zeros((self.orig_shape[0], self.orig_shape[1], self.tiles.shape[-1]))
        for i, tile in enumerate(self.tiles):
            x1 = self.windows[i][0]
            y1 = self.windows[i][1]
            matrix[y1:y1+tile.shape[1], x1:x1+tile.shape[0]] += tile
            stack_occur_matrix[y1:y1+tile.shape[1], x1:x1+tile.shape[0]] += 1
        return matrix // stack_occur_matrix
    
    def get_merged_overlapped_masks(self):
        
        stack_occur_matrix = np.zeros(self.orig_shape)
        matrix = np.zeros(self.orig_shape)
        
        for i, tile in enumerate(self.tiles):
            x1 = self.windows[i][0]
            y1 = self.windows[i][1]
            matrix[y1:y1+tile.shape[1], x1:x1+tile.shape[0]] += tile
            stack_occur_matrix[y1:y1+tile.shape[1], x1:x1+tile.shape[0]] += 1
            
        return matrix // stack_occur_matrix
    
    def get_tiles(self):
        return np.copy(self.tiles)
    
    def set_tiles(self, tiles):
        self.tiles = tiles
    
    def get_suffixes_string(self):
        return ['_'.join(map(str, tup)) for tup in self.windows]
    
    def plot_tiles(self, save_to="test_plot.png", figsize=(8,8)):
        if len(self.tiles) == 0:
            print("Error! Call cut() method first!")
            return
        fig, axs = plt.subplots(self.rows, self.cols, figsize=figsize, facecolor='w', edgecolor='k')
        fig.tight_layout(pad=0.01)

        for i, tile in enumerate(self.tiles):
            row = self.coords[i][0]
            col = self.coords[i][1]
            axs[col, row].set_xticks([]) 
            axs[col, row].set_yticks([])
            if len(tile.shape) == 2:
                axs[col, row].imshow(tile, cmap=plt.get_cmap('gray'))
            else:
                axs[col, row].imshow(tile)
        plt.show()
        if save_to:
            fig.savefig(save_to)