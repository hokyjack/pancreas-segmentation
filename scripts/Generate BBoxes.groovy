import qupath.lib.objects.PathAnnotationObject
import qupath.lib.objects.classes.PathClassFactory
import qupath.lib.roi.RectangleROI

// Define the size of the region to create
double sizeMicrons = 1000.0

// Get main data structures
def imageData = getCurrentImageData()
def server = imageData.getServer()

def cal = server.getPixelCalibration()

def imgHeight = Math.round(server.getHeight() * cal.getAveragedPixelSizeMicrons())
def imgWidth = Math.round(server.getWidth() * cal.getAveragedPixelSizeMicrons())

print("imgHeight: " + imgHeight)
print("imgWidth: " + imgWidth)



def randomBoxes = []

while(randomBoxes.size() < 50) {
    def r = new Random()
    randY = r.nextInt((int) (imgHeight/1000))
    randX = r.nextInt((int) (imgWidth/1000))
    if (!([randX, randY] in randomBoxes)) {
        randomBoxes.add([randX, randY])
    }
}

// Convert size in microns to pixels - QuPath ROIs are defined in pixel units of the full-resolution image
int sizePixels = Math.round(sizeMicrons / cal.getAveragedPixelSizeMicrons())

// Get the current viewer & the location of the pixel currently in the center
def viewer = getCurrentViewer()
//double cx = viewer.getCenterPixelX()
//double cy = viewer.getCenterPixelY()

for (box in randomBoxes) {

    double cx = box[0] * 1000 / cal.getAveragedPixelSizeMicrons()
    double cy = box[1] * 1000 / cal.getAveragedPixelSizeMicrons()
    
    // Create a new Rectangle ROI
    //def roi = new RectangleROI(cx-sizePixels/2, cy-sizePixels/2, sizePixels, sizePixels)
    def roi = new RectangleROI(cx, cy, sizePixels, sizePixels)

    // Create & new annotation & add it to the object hierarchy
    def annotation = new PathAnnotationObject(roi, PathClassFactory.getPathClass("BBox"))
    imageData.getHierarchy().addPathObject(annotation, true)
    annotation.setLocked(true)
}