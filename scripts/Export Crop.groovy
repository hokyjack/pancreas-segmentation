
import qupath.lib.images.servers.ImageServer
import qupath.lib.objects.PathObject

import javax.imageio.ImageIO
import java.awt.Color
import java.awt.image.BufferedImage

def objects = getAllObjects()

def imageData = getCurrentImageData()
def server = imageData.getServer()

def downsample = 2.0
dirname = "exported_crops"
pathOutput = buildFilePath(QPEx.PROJECT_BASE_DIR, dirname)
imagePathOutput = pathOutput
masksPathOutput = pathOutput
mkdirs(pathOutput)

def imageExportType = 'JPG'
def i = 0
rgbImageSaved = false

print 'Export Annotations started...'

print 'Finding BBoxes'
def annotations = getAnnotationObjects()
for (a in annotations) {
    def roi = a.getROI()
    def box_x1 = roi.getBoundsX()
    def box_x2 = roi.getBoundsX() + roi.getBoundsWidth()
    def box_y1 = roi.getBoundsY()
    def box_y2 = roi.getBoundsY() + roi.getBoundsHeight()
    
    def pathClass = a.getPathClass()
    def classificationName = pathClass == null ? 'None' : pathClass.toString()
    
    def numAnnotations = 0
    
    if (classificationName == 'crop') {
        
        pathOutput = buildFilePath(QPEx.PROJECT_BASE_DIR, dirname, server.getMetadata().getName() + "_" + i)
        imagePathOutput = buildFilePath(pathOutput, 'images')
        masksPathOutput = buildFilePath(pathOutput, 'masks')
        mkdirs(pathOutput)
        mkdirs(imagePathOutput)
        mkdirs(masksPathOutput)
        
        def region = RegionRequest.createInstance(server.getPath(), downsample,  (int) box_x1, (int) box_y1, (int) roi.getBoundsWidth(), (int) roi.getBoundsHeight())
        saveBBoxImage(region, server, imagePathOutput, downsample, imageExportType)
        
        // Export each annotation
        for (obj in objects) {

            if (obj == a) {
                //saveMask(obj, region, server, masksPathOutput, downsample, imageExportType)
            }
        }
    }
}




print 'Done!'

def saveBBoxImage (RegionRequest region, ImageServer server, String pathOutput, double downsample, String imageExportType) {

    String img_name = String.format('%s_(%.2f,%d,%d,%d,%d)',
            server.getMetadata().getName(),
            region.getDownsample(),
            region.getX(),
            region.getY(),
            region.getWidth(),
            region.getHeight()
    )
    
    def img = server.readBufferedImage(region)
    def fileImage = new File(pathOutput, img_name + '.' + imageExportType.toLowerCase())
    if (!rgbImageSaved) {
        ImageIO.write(img, imageExportType, fileImage)
    }

}

def saveMask(PathObject pathObject, RegionRequest region, ImageServer server, String pathOutput, double downsample, String imageExportType) {

    // Extract ROI & classification name
    def roi = pathObject.getROI()
    def pathClass = pathObject.getPathClass()
    def classificationName = pathClass == null ? 'None' : pathClass.toString()
    if (roi == null) {
        print 'Warning! No ROI for object ' + pathObject + ' - cannot export corresponding region & mask'
        return
    }
    
    
    // Create a name
    String name = String.format('[%s]_(%.2f,%.2f,%.2f,%.2f)',
            classificationName,
            roi.getBoundsX(),
            roi.getBoundsY(),
            roi.getBoundsX() + roi.getBoundsWidth(),
            roi.getBoundsY() + roi.getBoundsHeight()
    )
    
    print(name)

    // Request the BufferedImage
    def img = server.readBufferedImage(region)

    // Create a mask using Java2D functionality
    // (This involves applying a transform to a graphics object, so that none needs to be applied to the ROI coordinates)
    def shape = RoiTools.getShape(roi)
    def imgMask = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_BYTE_GRAY)
    def g2d = imgMask.createGraphics()
    g2d.setColor(Color.WHITE)
    g2d.scale(1.0/downsample, 1.0/downsample)
    g2d.translate(-region.getX(), -region.getY())
    g2d.fill(shape)
    g2d.dispose()

    // Export the mask
    def fileMask = new File(pathOutput, name + '-mask.png')
    ImageIO.write(imgMask, 'PNG', fileMask)

}