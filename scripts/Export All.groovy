import qupath.lib.images.servers.ImageServer
import qupath.lib.objects.PathObject

import qupath.lib.roi.RectangleROI
import qupath.lib.objects.PathAnnotationObject

import javax.imageio.ImageIO
import java.awt.Color
import java.awt.image.BufferedImage

// CONFIGURATION, CHANGING MAY BREAK THE SEGMENTATION RESULTS
downsample = 2.0
dirname = "export"
imageExportType = 'JPG'
// END CONFIGURATION

exportMasks = false
setImageType('BRIGHTFIELD_H_E');

// remove all existing annnotations
removeObjects(getAnnotationObjects(), false)

// parameters for detecting tissue samples in whole scan
detectParams = '{"threshold": 239,  "requestedPixelSizeMicrons": 70.0, ' +
               '"minAreaMicrons": 600000.0,  "maxHoleAreaMicrons": 2000000.0,' +
               '"darkBackground": false,  "smoothImage": true,  "medianCleanup": true,' + 
               '"dilateBoundaries": true,  "smoothCoordinates": true,' +
               '"excludeOnBoundary": false,  "singleAnnotation": false}'
// actual detection               
runPlugin('qupath.imagej.detect.tissue.SimpleTissueDetection2', detectParams);

// assigning "crop" class to each detected tissue
def cropClass = getPathClass('crop')
def ans = getAnnotationObjects()
for (a in ans) {
    def roi = a.getROI()
    def pathClass = a.getPathClass()
    def classificationName = pathClass == null ? 'None' : pathClass.toString()
    a.setPathClass(cropClass)
}

def objects = getAllObjects()
def imageData = getCurrentImageData()
def server = imageData.getServer()

dirname = buildFilePath(dirname, "scans")
pathOutput = dirname
imagePathOutput = pathOutput
masksPathOutput = pathOutput
mkdirs(pathOutput)
def i = 0
rgbImageSaved = false

print 'Export Annotations started...'

// save crops of tissue samples in 2.0 downsample
def annotations = getAnnotationObjects()
for (a in annotations) {
    def roi = a.getROI()
    def box_x1 = roi.getBoundsX()
    def box_x2 = roi.getBoundsX() + roi.getBoundsWidth()
    def box_y1 = roi.getBoundsY()
    def box_y2 = roi.getBoundsY() + roi.getBoundsHeight()
    
    def pathClass = a.getPathClass()
    def classificationName = pathClass == null ? 'None' : pathClass.toString()
    
    def numAnnotations = 0
    
    if (classificationName == 'crop') {
        
        pathOutput = buildFilePath(dirname, server.getMetadata().getName())
        imagePathOutput = buildFilePath(pathOutput, 'crops')
        
        mkdirs(pathOutput)
        mkdirs(imagePathOutput)

        if (exportMasks) {
            mkdirs(masksPathOutput)
            masksPathOutput = buildFilePath(pathOutput, 'masks')
        }
        
        def region = RegionRequest.createInstance(server.getPath(), downsample,  (int) box_x1, (int) box_y1, (int) roi.getBoundsWidth(), (int) roi.getBoundsHeight())
        saveBBoxImage(region, server, imagePathOutput, downsample, imageExportType)
        
        // Export each mask if enabled
        if (exportMasks) {
            for (obj in objects) {

                if (obj == a) {
                    saveMask(obj, region, server, masksPathOutput, downsample, imageExportType)
                }
            }
        }
    }
}

// save full scan image with 5.0 downsample
saveImage(server, pathOutput, 5.0, "JPG")
print 'Done!'



def saveBBoxImage (RegionRequest region, ImageServer server, String pathOutput, double downsample, String imageExportType) {

    String img_name = String.format('%s_(%.2f,%d,%d,%d,%d)',
            server.getMetadata().getName(),
            region.getDownsample(),
            region.getX(),
            region.getY(),
            region.getWidth(),
            region.getHeight()
    )
    
    def img = server.readBufferedImage(region)

    //pathOutput = buildFilePath(pathOutput, server.getMetadata().getName(), "crops")
    def fileImage = new File(pathOutput, img_name + '.' + imageExportType.toLowerCase())
    if (!rgbImageSaved) {
        ImageIO.write(img, imageExportType, fileImage)
    }

}


def saveImage (ImageServer server, String pathOutput, double downsample, String imageExportType) {

    // Create a new Rectangle ROI
    def roi = new RectangleROI(0, 0, server.getWidth(), server.getHeight())
    def box_x1 = roi.getBoundsX()
    def box_x2 = roi.getBoundsX() + roi.getBoundsWidth()
    def box_y1 = roi.getBoundsY()
    def box_y2 = roi.getBoundsY() + roi.getBoundsHeight()
    def region = RegionRequest.createInstance(server.getPath(), downsample,  (int) box_x1, (int) box_y1, (int) roi.getBoundsWidth(), (int) roi.getBoundsHeight())
    
    String img_name = String.format('%s_(%.2f,full)',
            server.getMetadata().getName(),
            region.getDownsample(),
    )
    
    def img = server.readBufferedImage(region)
    def fileImage = new File(pathOutput, img_name + '.' + imageExportType.toLowerCase())
    ImageIO.write(img, imageExportType, fileImage)

}

def saveMask(PathObject pathObject, RegionRequest region, ImageServer server, String pathOutput, double downsample, String imageExportType) {

    // Extract ROI & classification name
    def roi = pathObject.getROI()
    def pathClass = pathObject.getPathClass()
    def classificationName = pathClass == null ? 'None' : pathClass.toString()
    if (roi == null) {
        print 'Warning! No ROI for object ' + pathObject + ' - cannot export corresponding region & mask'
        return
    }
    
    
    // Create a name
    String name = String.format('[%s]_(%.2f,%.2f,%.2f,%.2f)',
            classificationName,
            roi.getBoundsX(),
            roi.getBoundsY(),
            roi.getBoundsX() + roi.getBoundsWidth(),
            roi.getBoundsY() + roi.getBoundsHeight()
    )
    
    print(name)

    // Request the BufferedImage
    def img = server.readBufferedImage(region)

    // Create a mask using Java2D functionality
    // (This involves applying a transform to a graphics object, so that none needs to be applied to the ROI coordinates)
    def shape = RoiTools.getShape(roi)
    def imgMask = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_BYTE_GRAY)
    def g2d = imgMask.createGraphics()
    g2d.setColor(Color.WHITE)
    g2d.scale(1.0/downsample, 1.0/downsample)
    g2d.translate(-region.getX(), -region.getY())
    g2d.fill(shape)
    g2d.dispose()

    // Export the mask
    maskPathOutput = buildFilePath(pathOutput, "masks")
    def fileMask = new File(pathOutput, name + '-mask.png')
    ImageIO.write(imgMask, 'PNG', fileMask)

}