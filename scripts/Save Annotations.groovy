import qupath.lib.objects.PathAnnotationObject

// Get the imageData & server
def imageData = getCurrentImageData()
def server = imageData.getServer()
// Get the file name from the current server
//def name = server.getShortServerName()
def name = server.getPath()

def path = buildFilePath(PROJECT_BASE_DIR, "saved_annotations")
mkdirs(path)

def serverPath = server.getPath()
def imgName = serverPath[serverPath.lastIndexOf('/')+1 .. serverPath.lastIndexOf(".") - 1]
def path2 = buildFilePath(PROJECT_BASE_DIR,"saved_annotations", imgName)
def annotations = getAnnotationObjects().collect {new PathAnnotationObject(it.getROI(), it.getPathClass())}
new File(path2).withObjectOutputStream {
it.writeObject(annotations)
}
print 'Done!'