import qupath.lib.objects.PathAnnotationObject

// Get the imageData & server
def imageData = getCurrentImageData()
def server = imageData.getServer()
// Get the file name from the current server
//def name = server.getShortServerName()
def name = server.getPath()

def serverPath = server.getPath()
def imgName = serverPath[serverPath.lastIndexOf('/')+1 .. serverPath.lastIndexOf(".") - 1]
def path = buildFilePath(PROJECT_BASE_DIR,"saved_annotations", imgName)

def annotations = null
new File(path).withObjectInputStream {
annotations = it.readObject()
}
addObjects(annotations)
print 'Added ' + annotations