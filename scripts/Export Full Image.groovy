
import qupath.lib.roi.RectangleROI
import qupath.lib.objects.PathAnnotationObject
import javax.imageio.ImageIO

// Define the size of the region to create
double downsample = 5.0
dirname = "exported_full"

// Get main data structures
def imageData = QPEx.getCurrentImageData()
def server = imageData.getServer()

// Convert size in microns to pixels - QuPath ROIs are defined in pixel units of the full-resolution image//

def saveImage (ImageServer server, String pathOutput, double downsample, String imageExportType) {

    // Create a new Rectangle ROI
    def roi = new RectangleROI(0, 0, server.getWidth(), server.getHeight())
    def box_x1 = roi.getBoundsX()
    def box_x2 = roi.getBoundsX() + roi.getBoundsWidth()
    def box_y1 = roi.getBoundsY()
    def box_y2 = roi.getBoundsY() + roi.getBoundsHeight()
    def region = RegionRequest.createInstance(server.getPath(), downsample,  (int) box_x1, (int) box_y1, (int) roi.getBoundsWidth(), (int) roi.getBoundsHeight())
    
    String img_name = String.format('%s_(%.2f,full)',
            server.getMetadata().getName(),
            region.getDownsample(),
    )
    
    def img = server.readBufferedImage(region)
    def fileImage = new File(pathOutput, img_name + '.' + imageExportType.toLowerCase())
    ImageIO.write(img, imageExportType, fileImage)

}


pathOutput = buildFilePath(QPEx.PROJECT_BASE_DIR, dirname)
mkdirs(pathOutput)
saveImage(server, pathOutput, downsample, "JPG")

