// remove all existing annnotations
removeObjects(getAnnotationObjects(), false)

detectParams = '{"threshold": 239,  "requestedPixelSizeMicrons": 70.0, ' +
               '"minAreaMicrons": 600000.0,  "maxHoleAreaMicrons": 2000000.0,' +
               '"darkBackground": false,  "smoothImage": true,  "medianCleanup": true,' + 
               '"dilateBoundaries": true,  "smoothCoordinates": true,' +
               '"excludeOnBoundary": false,  "singleAnnotation": false}'
               
runPlugin('qupath.imagej.detect.tissue.SimpleTissueDetection2', detectParams);

def cropClass = getPathClass('crop')
def annotations = getAnnotationObjects()
for (a in annotations) {
    def roi = a.getROI()
    def pathClass = a.getPathClass()
    def classificationName = pathClass == null ? 'None' : pathClass.toString()
    a.setPathClass(cropClass)
}
